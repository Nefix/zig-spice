{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    nativeBuildInputs = [
      pkgs.pkg-config
      pkgs.wabt
      pkgs.wasmer
      pkgs.spice_protocol
    ];
}
