const std = @import("std");
const channel = @import("channel.zig");

pub const Spice = struct {
    const This = @This();

    allocator: std.mem.Allocator,

    addr: []const u8,

    main: channel.Main,

    pub fn init(allocator: std.mem.Allocator, addr: []const u8) This {
        return This{
            .allocator = allocator,
            .addr = addr,
            .main = channel.Main.init(allocator, addr),
        };
    }

    pub fn tick(this: *This) void {
        this.main.tick();
    }

    pub fn deinit(this: *This) void {
        this.main.deinit();
    }
};

comptime {
    std.testing.refAllDecls(@This());
}
