const std = @import("std");

pub const Stream = struct {
    pub const ReadError = std.os.ReadError;
    pub const WriteError = std.os.WriteError;

    pub const State = enum {
        Closed,
        Open,
        Connecting,
        Closing,
    };

    impl: *anyopaque,
    stateFn: fn (*anyopaque) State,
    readFn: fn (*anyopaque, []u8) ReadError!usize,
    writeFn: fn (*anyopaque, []const u8) WriteError!usize,
    closeFn: fn (*anyopaque) void,

    pub fn state(iface: *const Stream) State {
        return iface.stateFn(iface.impl);
    }

    pub fn read(iface: *const Stream, buf: []u8) ReadError!usize {
        return iface.readFn(iface.impl, buf);
    }

    pub fn write(iface: *const Stream, buf: []const u8) WriteError!usize {
        return iface.writeFn(iface.impl, buf);
    }

    pub fn close(iface: *const Stream) void {
        return iface.closeFn(iface.impl);
    }
};
