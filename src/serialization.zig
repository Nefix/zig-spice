const std = @import("std");
const spice = @import("c.zig");

pub fn sizeOf(comptime T: type) comptime_int {
    const info = @typeInfo(T);

    switch (info) {
        .Struct => {
            var size: usize = 0;
            inline for (std.meta.fields(T)) |field| {
                size += sizeOf(field.field_type);
            }

            return size;
        },
        .Int => return @sizeOf(T),
        .Array => return @sizeOf(info.Array.child) * info.Array.len,
        else => @compileLog("unsupported type", T),
    }
}

test "sizeOf eats up all the padding bytes" {
    const h = .{ spice.SpiceLinkMess{
        .connection_id = 0,
        .channel_type = spice.SPICE_CHANNEL_MAIN,
        .channel_id = 0,
        .num_common_caps = 0,
        .num_channel_caps = 0,
        .caps_offset = 16,
    }, [_]u32{1 << spice.SPICE_COMMON_CAP_MINI_HEADER} };

    try std.testing.expectEqual(22, sizeOf(@TypeOf(h)));
}

pub fn Serializable(comptime T: type) type {
    return struct {
        const This = @This();

        const t_info = @typeInfo(T);
        const t_size = sizeOf(T);

        pub fn marshal(msg: T) [t_size]u8 {
            switch (t_info) {
                .Struct => {
                    var b: [t_size]u8 = undefined;
                    var size: usize = 0;

                    inline for (std.meta.fields(T)) |field| {
                        const F = field.field_type;
                        const f_size = sizeOf(F);

                        const val = Serializable(F).marshal(@field(msg, field.name));
                        std.mem.copy(u8, b[size .. size + f_size], val[0..]);

                        size += f_size;
                    }

                    return b;
                },
                .Array, .Int => return std.mem.toBytes(std.mem.nativeToLittle(T, msg)),
                else => @compileLog("unsupported marshal type", T),
            }
        }

        test "marshal should work as expected" {
            {
                const h = spice.SpiceLinkHeader{
                    .magic = spice.SPICE_MAGIC,
                    .major_version = spice.SPICE_VERSION_MAJOR,
                    .minor_version = spice.SPICE_VERSION_MINOR,
                    .size = 50,
                };
                const b = Serializable(@TypeOf(h)).marshal(h);

                try std.testing.expectEqual([_]u8{
                    // Magic
                    'R', 'E', 'D', 'Q',
                    // Major
                    2,   0,   0,   0,
                    // Minor
                    2,   0,   0,   0,
                    // Size
                    50,  0,   0,   0,
                }, b);
            }

            {
                const h = [_]u32{1 << spice.SPICE_COMMON_CAP_MINI_HEADER};
                const b = Serializable(@TypeOf(h)).marshal(h);

                try std.testing.expectEqual([_]u8{ 8, 0, 0, 0 }, b);
            }
        }

        pub fn unmarshal(buf: []u8) T {
            switch (t_info) {
                .Struct => {
                    comptime var size = 0;
                    var result: T = undefined;

                    inline for (std.meta.fields(T)) |field| {
                        const F = field.field_type;
                        const f_size = sizeOf(F);

                        const result_field = Serializable(F).unmarshal(buf[size .. size + f_size]);

                        @field(result, field.name) = result_field;

                        size += f_size;
                    }

                    return result;
                },
                .Array, .Int => return std.mem.bytesToValue(T, &std.mem.littleToNative([t_size]u8, buf[0..t_size].*)),
                else => @compileLog("unsupported unmarshal type", T),
            }
        }

        test "unmarshal" {
            {
                var b = [_]u8{
                    // Magic
                    'R', 'E', 'D', 'Q',
                    // Major
                    2,   0,   0,   0,
                    // Minor
                    2,   0,   0,   0,
                    // Size
                    50,  0,   0,   0,
                };
                const h = Serializable(spice.SpiceLinkHeader).unmarshal(&b);

                try std.testing.expectEqual(spice.SpiceLinkHeader{
                    .magic = spice.SPICE_MAGIC,
                    .major_version = spice.SPICE_VERSION_MAJOR,
                    .minor_version = spice.SPICE_VERSION_MINOR,
                    .size = 50,
                }, h);
            }

            {
                var b = [_]u8{ 3, 0, 0, 0, 4, 0, 0, 0, 5, 0, 0, 0 };

                const h = Serializable([3]u32).unmarshal(&b);

                try std.testing.expectEqual([_]u32{ 3, 4, 5 }, h);
            }

            // TODO: Make this work!
            // {
            //     const Point = extern struct {
            //         x: i32,
            //         y: i32,
            //     };
            //     const Line = struct {
            //         start: Point,
            //         end: Point,
            //         name: [8]u8,
            //     };

            //     var b = [_]u8{ 40, 0, 0, 0, 20, 0, 0, 0, 180, 0, 0, 0, 90, 0, 0, 0, 'n', 'e', 'f', 'i', 'x', 0, 0, 0 };
            //     const h = Serializable(Line).unmarshal(&b);

            //     try std.testing.expectEqual(Line{
            //         .start = Point{
            //             .x = 40,
            //             .y = 20,
            //         },
            //         .end = Point{
            //             .x = 180,
            //             .y = 90,
            //         },
            //         .name = [8]u8{ 'n', 'e', 'f', 'i', 'x', 0, 0, 0 },
            //     }, h);
            // }
        }
    };
}

pub fn marshalMsgs(msg: anytype) [sizeOf(@TypeOf(msg)) + sizeOf(spice.SpiceLinkHeader)]u8 {
    const T = @TypeOf(msg);
    const hdr_size = sizeOf(spice.SpiceLinkHeader);

    var b: [sizeOf(T) + hdr_size]u8 = undefined;

    var size: usize = hdr_size;
    inline for (std.meta.fields(T)) |field| {
        const F = field.field_type;
        const f_size = sizeOf(F);

        const b_field = Serializable(F).marshal(@field(msg, field.name));

        std.mem.copy(u8, b[size .. size + f_size], b_field[0..]);

        size += f_size;
    }

    const b_hdr = Serializable(spice.SpiceLinkHeader).marshal(spice.SpiceLinkHeader{
        .magic = spice.SPICE_MAGIC,
        .major_version = spice.SPICE_VERSION_MAJOR,
        .minor_version = spice.SPICE_VERSION_MINOR,
        .size = @intCast(u32, sizeOf(@TypeOf(msg))),
    });

    std.mem.copy(u8, b[0..b_hdr.len], b_hdr[0..]);

    return b;
}

test "marshalMsg works correctly" {
    {
        const b = marshalMsgs(spice.SpiceLinkMess{
            .connection_id = 0,
            .channel_type = spice.SPICE_CHANNEL_MAIN,
            .channel_id = 0,
            .num_common_caps = 0,
            .num_channel_caps = 0,
            .caps_offset = 16,
        });

        try std.testing.expectEqual([_]u8{
            // HEADER
            // Magic
            'R', 'E', 'D', 'Q',
            // Major
            2,   0,   0,   0,
            // Minor
            2,   0,   0,   0,
            // Size
            18,  0,   0,   0,

            // MESSAGE
            // Connection ID
            0,   0,   0,   0,
            // Channel type
            1,
            // Channel ID
              0,
            // Num common caps
              0,   0,
            0,   0,
            // Num channel caps
              0,   0,
            0,   0,
            // Caps offset
              16,  0,
            0,   0,
        }, b);
    }

    {
        const b = marshalMsgs(.{ spice.SpiceLinkMess{
            .connection_id = 0,
            .channel_type = spice.SPICE_CHANNEL_MAIN,
            .channel_id = 0,
            .num_common_caps = 0,
            .num_channel_caps = 0,
            .caps_offset = 16,
        }, [_]u32{1 << spice.SPICE_COMMON_CAP_MINI_HEADER} });

        try std.testing.expectEqual([_]u8{
            // HEADER
            // Magic
            'R', 'E', 'D', 'Q',
            // Major
            2,   0,   0,   0,
            // Minor
            2,   0,   0,   0,
            // Size
            22,  0,   0,   0,

            // MESSAGE
            // Connection ID
            0,   0,   0,   0,
            // Channel type
            1,
            // Channel ID
              0,
            // Num common caps
              0,   0,
            0,   0,
            // Num channel caps
              0,   0,
            0,   0,
            // Caps offset
              16,  0,
            0,   0,

            // Cap miniheader
              8,   0,
            0,   0,
        }, b);
    }
}

// pub fn marshalMsg(allocator: std.mem.Allocator, msg: anytype) std.mem.Allocator.Error![]const u8 {
//     const body = try marshal(allocator, msg);
//     defer allocator.free(body);
//     // TODO: errdefer maybe?

//     const h = try marshal(allocator, spice.SpiceLinkHeader{
//         .magic = spice.SPICE_MAGIC,
//         .major_version = spice.SPICE_VERSION_MAJOR,
//         .minor_version = spice.SPICE_VERSION_MINOR,
//         .size = @intCast(u32, body.len),
//     });
//     defer allocator.free(h);

//     return std.mem.concat(allocator, u8, &[_][]const u8{ h, body }) catch |err| {
//         std.debug.panic("concatenate header and message: {s}\n", .{err});
//     };
// }

// test "marshalMsg should work correctly" {
//     const b = try marshalMsg(std.testing.allocator, spice.SpiceLinkMess{
//         .connection_id = 0,
//         .channel_type = spice.SPICE_CHANNEL_MAIN,
//         .channel_id = 0,
//         .num_common_caps = 0,
//         .num_channel_caps = 0,
//         .caps_offset = 16,
//     });
//     defer std.testing.allocator.free(b);

//     try std.testing.expectEqualSlices(u8, &[_]u8{
//         // HEADER
//         // Magic
//         'R', 'E', 'D', 'Q',
//         // Major
//         2,   0,   0,   0,
//         // Minor
//         2,   0,   0,   0,
//         // Size
//         18,  0,   0,   0,

//         // MESSAGE
//         // Connection ID
//         0,   0,   0,   0,
//         // Channel type
//         1,
//         // Channel ID
//           0,
//         // Num common caps
//           0,   0,
//         0,   0,
//         // Num channel caps
//           0,   0,
//         0,   0,
//         // Caps offset
//           16,  0,
//         0,   0,
//     }, b);
// }

// pub fn marshalMsgs(allocator: std.mem.Allocator, args: anytype) std.mem.Allocator.Error![]const u8 {
//     const ArgsType = @TypeOf(args);
//     const fields_info = std.meta.fields(ArgsType);

//     var bMsgs = comptime std.mem.zeroes([fields_info.len][]const u8);

//     inline for (fields_info) |field, i| {
//         bMsgs[i] = try marshal(allocator, @field(args, field.name));
//     }

//     const b = try std.mem.concat(allocator, u8, bMsgs[0..]);
//     defer allocator.free(b);

//     for (bMsgs) |msg| {
//         allocator.free(msg);
//     }

//     const h = try marshal(allocator, spice.SpiceLinkHeader{
//         .magic = spice.SPICE_MAGIC,
//         .major_version = spice.SPICE_VERSION_MAJOR,
//         .minor_version = spice.SPICE_VERSION_MINOR,
//         .size = @intCast(u32, b.len),
//     });
//     defer allocator.free(h);

//     return try std.mem.concat(allocator, u8, &[_][]const u8{ h, b });
// }

// test "marshalMsgs should work correctly" {
// }

comptime {
    std.testing.refAllDecls(@This());
}
