const std = @import("std");
const builtin = @import("builtin");
const Spice = @import("spice.zig").Spice;

var gpa: std.heap.GeneralPurposeAllocator(.{}) = undefined;
var s: Spice = undefined;

export fn init() void {
    gpa = std.heap.GeneralPurposeAllocator(.{}){};
    s = Spice.init(gpa.allocator(), "wss://demo.isardvdi.com/isard-hypervisor/5900");
}

export fn deinit() void {
    s.deinit();
    _ = gpa.deinit();
}

export fn tick() void {
    s.tick();
}

fn mainWasi() callconv(.C) i32 {
    return 0;
}

comptime {
    if (builtin.os.tag == .wasi and !builtin.is_test) @export(mainWasi, .{ .name = "main" });
}

comptime {
    std.testing.refAllDecls(@This());
}
