const std = @import("std");
const net = @import("net.zig");

pub const Stream = struct {
    const This = @This();

    allocator: std.mem.Allocator,
    state: net.Stream.State = .Closed,
    buf: std.ArrayList(u8),

    pub fn init(allocator: std.mem.Allocator, url: []const u8) *This {
        var stream = allocator.create(This) catch unreachable;
        stream.allocator = allocator;
        stream.buf = std.ArrayList(u8).init(allocator);

        ws.init(stream, url.ptr, url.len);

        return stream;
    }

    pub fn deninit(this: *This) void {
        this.buf.deinit();
        this.allocator.destroy(this);
    }

    pub fn interface(this: *This) net.Stream {
        return .{
            .impl = @ptrCast(*anyopaque, this),
            .stateFn = stateFn,
            .readFn = read,
            .writeFn = write,
            .closeFn = close,
        };
    }

    fn stateFn(this_anyopaque: *anyopaque) net.Stream.State {
        var this = @ptrCast(*This, @alignCast(@alignOf(This), this_anyopaque));

        return this.state;
    }

    fn close(this_anyopaque: *anyopaque) void {
        var this = @ptrCast(*This, @alignCast(@alignOf(This), this_anyopaque));

        ws.close(this);
    }

    fn read(this_anyopaque: *anyopaque, buf: []u8) net.Stream.ReadError!usize {
        var this = @ptrCast(*This, @alignCast(@alignOf(This), this_anyopaque));

        if (this.buf.items.len < buf.len) {
            return std.os.ReadError.InputOutput;
        }

        var i: usize = 0;
        while (i < buf.len) : (i += 1) {
            const item = this.buf.orderedRemove(0);
            buf[i] = item;
        }

        return i;
    }

    fn write(this_anyopaque: *anyopaque, buffer: []const u8) net.Stream.WriteError!usize {
        var this = @ptrCast(*This, @alignCast(@alignOf(This), this_anyopaque));

        ws.send(this, buffer.ptr, buffer.len);
        return buffer.len;
    }
};

const ws = struct {
    pub extern "ws" fn init(stream: *Stream, url: [*]const u8, len: usize) void;
    pub extern "ws" fn close(stream: *Stream) void;
    pub extern "ws" fn send(stream: *Stream, msg: [*]const u8, len: usize) void;
};

export fn wsGetRecvBuf(stream: *Stream, len: usize) [*]u8 {
    const recvBuf = stream.allocator.alloc(u8, len) catch unreachable;

    return recvBuf.ptr;
}

export fn wsOnOpen(stream: *Stream) void {
    stream.state = .Open;
    std.log.debug("stream opened", .{});
    std.log.debug("{}", .{stream.state});
}

export fn wsOnClose(stream: *Stream) void {
    stream.state = .Closing;
    std.log.debug("stream closed", .{});
}

export fn wsOnMsg(stream: *Stream, msg: [*]u8, len: usize) void {
    std.log.debug("new msg!: {d}", .{msg[0..len]});
    stream.buf.appendSlice(msg[0..len]) catch |err| {
        std.debug.panic("add message to wasm stream: {s}\n", .{err});
    };
}
