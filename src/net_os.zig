const std = @import("std");
const net = @import("net.zig");

pub const Stream = struct {
    const This = @This();

    state: net.Stream.State = .Closed,
    stream: std.net.Stream,

    pub fn init(allocator: std.mem.Allocator, host: []const u8, port: u16) This {
        var stream = std.net.tcpConnectToHost(allocator, host, port) catch |err| {
            std.debug.panic("connect to host: {}", .{err});
        };

        return This{
            .stream = stream,
        };
    }

    pub fn interface(this: *This) net.Stream {
        return .{
            .impl = @ptrCast(*anyopaque, this),
            .stateFn = stateFn,
            .readFn = read,
            .writeFn = write,
            .closeFn = close,
        };
    }

    fn stateFn(this_anyopaque: *anyopaque) net.Stream.State {
        var this = @ptrCast(*This, @alignCast(@alignOf(This), this_anyopaque));

        return this.state;
    }

    fn close(this_anyopaque: *anyopaque) void {
        var this = @ptrCast(*This, @alignCast(@alignOf(This), this_anyopaque));

        this.stream.close();
    }

    fn read(this_anyopaque: *anyopaque, buf: []u8) net.Stream.ReadError!usize {
        var this = @ptrCast(*This, @alignCast(@alignOf(This), this_anyopaque));

        return try this.stream.reader().readAll(buf);
    }

    fn write(this_anyopaque: *anyopaque, buffer: []const u8) net.Stream.WriteError!usize {
        var this = @ptrCast(*This, @alignCast(@alignOf(This), this_anyopaque));

        return try this.stream.write(buffer);
    }
};
