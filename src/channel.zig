const std = @import("std");
const serialization = @import("serialization.zig");
const spice = @import("c.zig");
const net = @import("net.zig");

fn Channel(chan_type: u8, comptime cmn_caps_len: usize, cmn_caps: [cmn_caps_len]u32, comptime chan_caps_len: usize, chan_caps: [chan_caps_len]u32) type {
    return struct {
        const This = @This();
        const chan_type = chan_type;

        allocator: std.mem.Allocator,
        addr: []const u8,
        stream: net.Stream,

        state: State = .Closed,
        conn_id: u8,
        chan_id: u8,

        pub const State = enum {
            Closed,
            Started,
            Linked,
            Ticketed,
            Ready,
            Err,
        };

        pub fn init(allocator: std.mem.Allocator, addr: []const u8, conn_id: u8, chan_id: u8) This {
            const stream: net.Stream = switch (@import("builtin").os.tag) {
                .wasi => @import("net_wasm.zig").Stream.init(allocator, addr).interface(),
                else => blk: {
                    var split_addr = std.mem.split(u8, addr, ":");

                    const host = split_addr.next().?;
                    const port = std.fmt.parseInt(u16, split_addr.next().?, 10) catch |err| {
                        std.debug.panic("parse port: {s}\n", .{err});
                    };

                    break :blk @import("net_os.zig").Stream.init(allocator, host, port).interface();
                },
            };

            return This{
                .allocator = allocator,
                .addr = addr,
                .stream = stream,
                .conn_id = conn_id,
                .chan_id = chan_id,
            };
        }

        pub fn tick(this: *This) void {
            if (this.stream.state() != net.Stream.State.Open) return;

            switch (this.state) {
                .Closed => this.start(),
                .Started => this.link(),
                else => std.debug.print("not closed!\n", .{}),
            }
        }

        fn start(this: *This) void {
            const msg = spice.SpiceLinkMess{
                .connection_id = this.conn_id,
                .channel_type = chan_type,
                .channel_id = this.chan_id,
                .num_common_caps = @intCast(u32, cmn_caps_len),
                .num_channel_caps = @intCast(u32, chan_caps_len),
                .caps_offset = 18,
            };

            const b = serialization.marshalMsgs(.{ msg, cmn_caps, chan_caps });

            _ = this.stream.write(b[0..]) catch |err| {
                std.debug.panic("write link message: {s}\n", .{err});
            };

            this.state = .Started;
        }

        fn link(this: *This) void {
            var hdrBuf: [16]u8 = undefined;

            _ = this.stream.read(&hdrBuf) catch |err| {
                std.debug.panic("read linked header: {s}\n", .{err});
            };

            const hdr = serialization.Serializable(spice.SpiceLinkHeader).unmarshal(&hdrBuf);

            std.debug.print("{}", .{hdr});

            var buf = this.allocator.alloc(u8, hdr.size) catch {
                std.debug.panic("allocate space for linked message", .{});
            };

            _ = this.stream.read(buf) catch |err| {
                std.debug.panic("read linked message: {s}\n", .{err});
            };

            const msg = serialization.Serializable(spice.SpiceLinkReply).unmarshal(buf);

            std.log.debug("{}", .{msg});
        }
    };
}

pub const Main = struct {
    const This = @This();

    const chan_type = spice.SPICE_CHANNEL_MAIN;
    const cmn_caps = [_]u32{1 << spice.SPICE_COMMON_CAP_MINI_HEADER};
    const chan_caps = [_]u32{};
    const Chan = Channel(chan_type, cmn_caps.len, cmn_caps, chan_caps.len, chan_caps);

    allocator: std.mem.Allocator,

    addr: []const u8,
    chan: Chan,

    pub fn init(allocator: std.mem.Allocator, addr: []const u8) This {
        return This{
            .allocator = allocator,
            .addr = addr,
            .chan = Chan.init(allocator, addr, 0, 0),
        };
    }

    pub fn deinit(_: *This) void {
        // this.chan.deinit();
    }

    pub fn tick(this: *This) void {
        this.chan.tick();
    }
};

comptime {
    std.testing.refAllDecls(@This());
}
