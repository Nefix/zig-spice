const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const lib = b.addSharedLibrary("zig-wasm", "src/main.zig", .unversioned);
    lib.setTarget(target);
    lib.setBuildMode(mode);

    lib.linkLibC();
    // TODO: This shouldn't be a system library, but a submodule
    lib.linkSystemLibrary("spice-protocol");

    lib.install();

    const tests = b.addTest("src/main.zig");

    tests.setTarget(target);
    tests.setBuildMode(mode);

    tests.linkLibC();
    tests.linkSystemLibrary("spice-protocol");

    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&tests.step);
}
