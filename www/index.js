import { WASI } from "@wasmer/wasi";
import { lowerI64Imports } from "@wasmer/wasm-transformer"

import { WasmFs } from "@wasmer/wasmfs";

const utf8decoder = new TextDecoder();
const utf8encoder = new TextEncoder();

function createPlatformEnvironment(getInstance) {
  const getMemory = () => getInstance().exports.memory

  let log_string = ''
  return {
    wasm_quit() {
      throw 'application exit'
    },
    wasm_panic: (ptr, len) => {
      let msg = utf8decoder.decode(new Uint8Array(getMemory().buffer, ptr, len))
      throw Error(msg)
    },
    wasm_log_write: (ptr, len) => {
      log_string += utf8decoder.decode(
        new Uint8Array(getMemory().buffer, ptr, len),
      )
    },
    wasm_log_flush: () => {
      console.log(log_string)
      log_string = ''
    },

    now_f64() {
      return Date.now()
    },
  }
}

const socketMap = {};
const createWebSocketModule = (getInstance) => {
  const getExports = () => getInstance().exports

  const bufToStr = (ptr, len) => {
    return utf8decoder.decode(new Uint8Array(getExports().memory.buffer, ptr, len))
  }

  const strToBuf = (str) => {
    return utf8encoder.encode(str)
  }

  return {
    init: (sock, url, len) => {
      const ws = new WebSocket(bufToStr(url, len))
      ws.binaryType = 'arraybuffer'
      ws.onopen = () => getExports().wsOnOpen(sock);
      ws.onclose = () => getExports().wsOnClose(sock);
      ws.onmessage = (event) => {
        const msg = new Uint8Array(event.data);
        const recvBufPtr = getExports().wsGetRecvBuf(sock, msg.length);
        const recvBuf = new Uint8Array(getExports().memory.buffer, recvBufPtr, msg.length);

        recvBuf.set(msg);

        getExports().wsOnMsg(sock, recvBufPtr, recvBuf.length);
      }

      socketMap[sock] = ws;
    },

    close: (sock) => {
      socketMap[sock].close();
      delete socketMap[sock];
    },

    send: (sock, msg, len) => socketMap[sock].send(new Uint8Array(getExports().memory.buffer, msg, len))
  }
}

var globalInstance;
let imports = {
  env: createPlatformEnvironment(() => globalInstance),
  ws: createWebSocketModule(() => globalInstance),
}

// Instantiate a new WASI Instance
const wasmFs = new WasmFs();
wasmFs.getStdErr = async () => {
  const promise = new Promise(resolve => {
    resolve(wasmFs.fs.readFileSync('/dev/stderr', 'utf8'));
  });
  return promise;
}

let wasi = new WASI({
  args: [],
  env: {},
  bindings: {
    // uses browser APIs in the browser, node APIs in node
    ...WASI.defaultBindings,
    fs: wasmFs.fs
  }
});

const startWasm = async () => {
  // Fetch our Wasm File
  const response = await fetch("../zig-out/lib/zig-wasm.wasm");
  const responseArrayBuffer = await response.arrayBuffer();

  // Instantiate the WebAssembly file
  const wasm_bytes = new Uint8Array(responseArrayBuffer).buffer;
  const lowered_wasm = await lowerI64Imports(wasm_bytes);
  let module = await WebAssembly.compile(lowered_wasm);
  let instance = await WebAssembly.instantiate(module, {
    ...wasi.getImports(module),
    ...imports
  });
  wasi.setMemory(instance.exports.memory)

  globalInstance = instance;

  instance.exports.init();

  const tick = async () => {
    try {
      instance.exports.tick();
    } catch {
      await wasmFs.getStdOut().then(rsp => {
        document.getElementById('out').innerText = rsp;
      });

      await wasmFs.getStdErr().then(rsp => {
        document.getElementById('err').innerText = rsp;
      })
    }

    await wasmFs.getStdOut().then(rsp => {
      document.getElementById('out').innerText = rsp;
    });

    await wasmFs.getStdErr().then(rsp => {
      document.getElementById('err').innerText = rsp;
    })


    window.requestAnimationFrame(tick)
  }

  window.requestAnimationFrame(tick)
};

startWasm();